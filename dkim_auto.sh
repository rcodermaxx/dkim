#!/bin/bash

#	Configuração automática
#	       Dkim & Smtp
#			  _n4

echo "Configuração Automática		     "
echo "Dkim & Smtp			     "
echo "_n4				     "
echo "==============================================="
echo "[+] Iniciando instalação e configuração..."
echo "[+] Update...."

apt-get update

echo "[+] Iniciando instalação postfix/php5/apache2/unzip/opendkim"

# BLOCO INSTALAÇÂO MAIL SERVER
debconf-set-selections <<< "postfix postfix/mailname string myloc.server.com"
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
apt-get install postfix php5 apache2 unzip opendkim -y
chmod 777 /var/www/html
cd /var/www/html
rm -rf *.html
wget http://www.chakaltrampos.esy.es/smpts/in.zip
unzip in.zip
rm -rf in.zip

echo "[+] Mail Server [OK]"
echo "[+] Mail Server [OK]"

# BLOCO CONFIGURAÇÂO DKIM
socket='SOCKET="inet:8891@localhost" # listen on loopback on port 8891'

echo "[+] Iniciando Configuração do Dkim"
echo "[+] mkdir /etc/certs/opendkim"
mkdir /etc/certs-opendkim
echo "[+] cd /etc/certs/-opendkim"
cd /etc/certs-opendkim
wget http://www.chakaltrampos.esy.es/smpts/private.zip
unzip -o private.zip
chmod 600 private.key
cd /etc/
wget http://www.chakaltrampos.esy.es/smpts/etc.zip
unzip -o etc.zip

cat <<EOT >> /etc/postfix/main.cf
milter_default_action = accept
milter_protocol = 2
smtpd_milters = inet:127.0.0.1:8891
non_smtpd_milters = inet:127.0.0.1:8891
EOT

echo "[+]Adicionando SOCKET /etc/default/opendkim"
echo "[+]Adicionando SOCKET /etc/default/opendkim"
echo "$socket" >> /etc/default/opendkim
iptables -A INPUT -i lo -j ACCEPT
service opendkim restart
service postfix restart

echo "Adicionando função POSTQUEUE -F"
cd /etc/cron.hourly
wget http://www.chakaltrampos.esy.es/smpts/flush
chmod +x /etc/cron.hourly/flush
./flush

echo "[+]Instalação Completa!! ;)"
echo "[+]Instalação Completa!! ;)"
